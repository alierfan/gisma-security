import mongoose from 'mongoose';
import { app } from './app';
import env from 'dotenv';

const start = async () => {

  env.config();
    try {
      await mongoose.connect(process.env.MONGO_URI || 'mongodb://127.0.0.1:27017/mydatabase', { 
      });
      console.log('connected to mongoose');
    } catch (err) {
      console.log(err);
    }
  
    app.listen(3000, () => {
      console.log('Listening on port 3000!!!!!!');
    });
  }


start();