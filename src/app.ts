import express, {Request, Response} from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import { currentUserRouter } from './routes/current-user';
import { signinRouter } from './routes/signin';
import { signupRouter } from './routes/signup';
import { errorHandler, NotFoundError } from '@alierfantickets/common';
import { authenticateToken } from './middlewares/authentication';


const app = express();
app.set('trust proxy', true); 
app.use(json());

app.use(currentUserRouter);
app.use(signinRouter);
app.use(signupRouter);

app.get("/api/users/user", authenticateToken, (req: Request, res: Response) => {

  return res.send('Authorized user!');

});

app.all('*', async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

export { app };